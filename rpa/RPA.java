/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rpa;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfDocument;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Graphics2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jopendocument.model.OpenDocument;
import org.jopendocument.renderer.ODTRenderer;

/**
 *
 * @author PLINIO
 */
public class RPA {

    public static void main(String[] args) {
        try {
            OpenDocument od = new OpenDocument();
            od.loadFrom("\\\\Qse-server\\compartilhada\\@DOCUMENTOS\\@MODELOS\\RECIBO DE PAGAMENTO A AUTONOMO - RPA\\4APP\\Editando\\2nafolha.odf");
            System.out.println("OpenDocument instanciado ...");
            Document d = new Document(PageSize.A4);
            File fo = new File("C:\\Users\\PLINIO\\Documents\\Apps\\RPA\\src\\teste.pdf");
            System.out.println("File output relacionado ...");
            PdfDocument pdf = new PdfDocument();
            d.addDocListener(pdf);
            System.out.println("PdfDocument instanciado ...");

            FileOutputStream fos = new FileOutputStream(fo);
            PdfWriter pdfw = PdfWriter.getInstance(pdf, fos);
            pdf.addWriter(pdfw);

            d.open();
            System.out.println("Abrindo documento ...");

            Rectangle pageSize = d.getPageSize();
            int w = (int) (pageSize.getWidth() * 0.9);
            int h = (int) (pageSize.getHeight() * 0.95);
            PdfContentByte cb = pdfw.getDirectContent();
            PdfTemplate tp = cb.createTemplate(w, h);
            System.out.println("Template do PDF criado.");

            Graphics2D g2 = tp.createPrinterGraphics(w, h, null);

            tp.setWidth(w);
            tp.setHeight(h);

            ODTRenderer r = new ODTRenderer(od);
            r.setIgnoreMargins(true);
            r.setPaintMaxResolution(true);
            System.out.println("Renderizador iniciado ...");

            r.setResizeFactor(r.getPrintWidth() / w);
            r.paintComponent(g2);
            g2.dispose();

            float offsetX = (pageSize.getWidth() - w) / 2;
            float offsetY = (pageSize.getHeight() - h) / 2;
            cb.addTemplate(tp, offsetX, offsetY);
            System.out.println("Adicionando template ...");
            d.close();
            System.out.println("Finalizado ...");

        } catch (DocumentException ex) {
            Logger.getLogger(RPA.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RPA.class.getName()).log(Level.SEVERE, null, ex);
        }


    }
}
